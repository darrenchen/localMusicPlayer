//
//  musicModel.swift
//  MusicPlay
//
//  Created by darren on 16/8/29.
//  Copyright © 2016年 darren. All rights reserved.
//

import UIKit

class musicModel: NSObject {

    var ID: String?
    var Title: String?
    var FileName: String?
    var LRC: String?
    var Sequence: String?
    var APP: String?

    
    init(dict: [String: AnyObject]) {
        super.init()
        
        ID = dict["ID"] as? String
        Title = dict["Title"] as? String
        FileName = dict["FileName"] as? String
        LRC = dict["LRC"] as? String
        Sequence = dict["Sequence"] as? String
        APP = dict["APP"] as? String
    }

}
