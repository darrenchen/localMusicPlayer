
import UIKit

class MusicMessageModel: NSObject {

    var musicM: musicModel?
    
    // 已经播放时间
    var costTime: Float64 = 0
    
    // 总时长
    var totalTime: Float64 = 0
    
    // 播放状态
    var isPlaying: Bool = false
}
