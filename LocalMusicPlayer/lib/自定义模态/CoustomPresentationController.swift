//
//  CoustomPresentationController.swift
//  CLKuGou_Swift
//
//  Created by Darren on 16/8/9.
//  Copyright © 2016年 darren. All rights reserved.
//

import UIKit

class CoustomPresentationController: UIPresentationController {

    override func presentationTransitionWillBegin() {
        let duration = UIDevice.current.orientation
        if duration == .portrait {  // 竖屏
            self.presentedView?.frame = CGRect(x: 0, y: 0,width: APPW, height: APPH)
            self.containerView?.addSubview(self.presentedView!)
        } else {
            self.presentedView?.frame = CGRect(x: 0, y: 0,width: APPH, height: APPW)
            self.containerView?.addSubview(self.presentedView!)
        }
        
    }
    
    override func dismissalTransitionWillBegin() {
    }
    
    override func presentationTransitionDidEnd(_ completed: Bool) {
        
    }
    
    override func dismissalTransitionDidEnd(_ completed: Bool) {
        presentedView?.removeFromSuperview()
    }
}
