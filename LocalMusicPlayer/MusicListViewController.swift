//
//  MusicListViewController.swift
//  LocalMusicPlayer
//
//  Created by Darren on 16/9/24.
//  Copyright © 2016年 darren. All rights reserved.
//

import UIKit

class MusicListViewController: UIViewController {

    @IBOutlet weak var musicListTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(white: 0,alpha: 0.5)

        musicListTableView.delegate = self
        musicListTableView.dataSource = self
        
        CLNotificationCenter.addObserver(self, selector: #selector(musicPlayed(_:)), name: NSNotification.Name(rawValue: "musicPlay"), object: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        musicListTableView.reloadData()
    }
    func musicPlayed(_ notic:Notification){
        self.musicListTableView.reloadData()
    }

    @IBAction func clickCloseBtn() {
        self.dismiss(animated: true, completion: nil)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // 隐藏状态栏
    override var prefersStatusBarHidden : Bool {
        return true
    }

}
extension MusicListViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MusicOperationTool.shareInstance.musicMs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ID = "musicListCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: ID)
        if cell == nil {
            cell = Bundle.main.loadNibNamed("musicListCell", owner: nil, options: nil)?.last as! musicListCell
        }
        (cell as! musicListCell).selectionStyle = .none
        (cell as! musicListCell).backgroundColor = UIColor.clear
        let model = MusicOperationTool.shareInstance.musicMs[(indexPath as NSIndexPath).row]
        if model.ID ==  MusicOperationTool.shareInstance.currentModel.ID {
            let indexPath = IndexPath.init(row: (indexPath as NSIndexPath).row, section: 0)
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
        (cell as! musicListCell).model = model
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.scrollToRow(at: indexPath, at: .middle, animated: true)
        
        let model = MusicOperationTool.shareInstance.musicMs[(indexPath as NSIndexPath).row]
        
        MusicOperationTool.shareInstance.currentModel = model;
        MusicOperationTool.shareInstance.currentPlayIndex = (indexPath as NSIndexPath).row
        MusicOperationTool.shareInstance.playMusic(model)
        
        CLNotificationCenter.post(name: Notification.Name(rawValue: "clickMusicListItem"), object: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
