//
//  CLLrcLable.swift
//  MusicPlay
//
//  Created by darren on 16/8/29.
//  Copyright © 2016年 darren. All rights reserved.
//

import UIKit

class CLLrcLable: UILabel {

    var progress:CGFloat = 0.0 {
        didSet{
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.drawText(in: rect)
        // 1.获取需要画的区域
        let fillRect = CGRect(x: 0, y: 0, width: self.bounds.size.width * self.progress, height: self.bounds.size.height)
        // 2.设置颜色  14699180
        CoustomColor(146,g: 99,b: 180,a: 1).set()
        // 3.添加区域
        UIRectFillUsingBlendMode(fillRect, .sourceIn)
    }

}
