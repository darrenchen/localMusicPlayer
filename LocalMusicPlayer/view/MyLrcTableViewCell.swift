//
//  MyLrcTableViewCell.swift
//  MusicPlay
//
//  Created by darren on 16/8/29.
//  Copyright © 2016年 darren. All rights reserved.
//

import UIKit

class MyLrcTableViewCell: UITableViewCell {

    var lrcLable = CLLrcLable()
    
    var cellprogress:CGFloat = 0.0{
        didSet{
            lrcLable.progress = cellprogress
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        lrcLable.frame = CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width-20, height: 40)
        lrcLable.textAlignment = .left
        self.contentView.addSubview(lrcLable)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
