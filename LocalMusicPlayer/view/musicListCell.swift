//
//  musicListCell.swift
//  MusicPlay
//
//  Created by Darren on 16/9/4.
//  Copyright © 2016年 darren. All rights reserved.
//

import UIKit

class musicListCell: UITableViewCell {

    var model = musicModel(dict:[:]) {
        didSet{
            guard let title = model.Title else {
                return
            }
            self.titleLable?.text = title
        }
    }
        
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var titleLable: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            self.titleLable?.textColor = CoustomColor(146,g: 99,b: 180,a: 1)
            self.iconView?.isHidden = false
            self.rightView.isHidden = false

            self.rightView.backgroundColor = UIColor.clear
            // 创建复制图层
            let repL = CAReplicatorLayer()
            repL.frame = CGRect(x: 0, y: 30, width: 24, height: self.rightView.cl_height)
            rightView.layer.addSublayer(repL)
            let layer = CALayer()
            layer.anchorPoint = CGPoint(x: 0, y: 1)
            layer.bounds = CGRect(x: 0,y: 0, width: 2,height: 20)
            layer.backgroundColor = UIColor.white.cgColor
            repL.addSublayer(layer)
            let anim = CABasicAnimation()
            anim.keyPath = "transform.scale.y"
            anim.toValue = 0.1
            anim.duration = 0.5
            anim.repeatCount = MAXFLOAT
            // 设置动画反转
            anim.autoreverses = true;
            layer.add(anim, forKey: nil)
            // 复制层中子层总数
            // instanceCount：表示复制层里面有多少个子层，包括原始层
            repL.instanceCount = 4
            // 设置复制子层偏移量，不包括原始层,相对于原始层x偏移
            repL.instanceTransform = CATransform3DMakeTranslation(4, 0, 0)
            // 设置复制层动画延迟时间
            repL.instanceDelay = 0.1
            // 如果设置了原始层背景色，就不需要设置这个属性
            repL.instanceColor = CoustomColor(146,g: 99,b: 180,a: 1).cgColor;
        } else {
            self.iconView?.isHidden = true
            self.titleLable?.textColor = UIColor.white
            
            self.rightView.isHidden = true
        }
    }
    
}
