

import UIKit

class MusicDataTool: NSObject {

    class func getMusicData(_ result: ([musicModel])->()) {
                
        let path: String = Bundle.main.path(forResource: "music.json", ofType:nil)!
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path))
        //读取Json数据
        let dictArr = try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)
        
        var musicArr:Array<musicModel> = []

        for item in dictArr as! [[String: AnyObject]] {
            let model = musicModel(dict: item )
            musicArr.append(model)
        }
        
        // 4. 返回结果
        result(musicArr)
    }
}
