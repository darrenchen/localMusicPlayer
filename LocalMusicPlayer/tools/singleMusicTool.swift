

// 这个类, 负责, 单首歌曲的操作, 播放, 暂停, 停止, 快进, 倍数
import UIKit
import AVFoundation

class singleMusicTool: NSObject {

    var player: AVPlayer?
    
    func playMusic(_ musicName: String?) -> () {
        
        // 1. 创建播放器
        guard let url = Bundle.main.url(forResource: musicName, withExtension: nil) else {return}
        
        player = AVPlayer(url:url)
        
        player?.play()
    }
    func pauseMusic() -> () {
        player?.pause()
    }
}
