//
//  CALayer+PauseAimate.swift
//  MusicPlay
//
//  Created by Darren on 16/9/9.
//  Copyright © 2016年 darren. All rights reserved.
//

import QuartzCore
extension CALayer {
    
    // 暂停动画
    func pauseAnimate(){
        let pausedTime = self.convertTime(CACurrentMediaTime(), to: nil)
        self.speed = 0
        self.timeOffset = pausedTime
    }
    
    // 恢复动画
    func resumeAnimate(){
        let pausedTime = self.timeOffset
        self.speed = 1
        self.timeOffset = 0
        self.beginTime = 0
        let timeSincePause = self.convertTime(CACurrentMediaTime(), to: nil)-pausedTime
        self.beginTime = timeSincePause
    }

}
