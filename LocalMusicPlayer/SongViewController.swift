//
//  SongViewController.swift
//  LocalMusicPlayer
//
//  Created by Darren on 16/9/23.
//  Copyright © 2016年 darren. All rights reserved.
//

import UIKit
import AVFoundation

class SongViewController: UIViewController {

    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var lrcTableView: UITableView!
    
    /**当前行*/
    var currentIndex = 0
    /**歌词数组*/
    var lrcArr = [String]()
    /**歌词时间数组*/
    var lycTimerArr = [String]()
    /**歌词定时器*/
    fileprivate var lrcTimer = CADisplayLink()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lrcTableView.delegate = self
        self.lrcTableView.dataSource = self
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(90 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            self.view.insertSubview(self.toolBar, belowSubview: self.iconView)
        }
        let duration = UIDevice.current.orientation
        if duration == .portrait {  // 竖屏
            self.lrcTableView.contentInset = UIEdgeInsetsMake(APPH*0.5, 0,APPH*0.5, 0);
        }
        // 监听状态，播放完成后自动播放下一首
        NotificationCenter.default.addObserver(self, selector: #selector(SongViewController.myMovieFinishedCallback), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: MusicOperationTool.shareInstance.tool.player?.currentItem)
    }
    override var prefersStatusBarHidden : Bool {
        return true;
    }
    func myMovieFinishedCallback(){
        self.updateMusicLrcInfo()
    }
    // 更新歌曲歌词信息
    func updateMusicLrcInfo(){
        self.lrcTableView.reloadData()
        self.currentIndex = 0
        // 开启歌词的定时器
        self.startLrcTime()
        self.setupLrcArr(MusicOperationTool.shareInstance.currentModel)
        self.lrcTableView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.setupLrcArr(MusicOperationTool.shareInstance.currentModel)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func timeStringWithString(_ timeString:String)->Float{
        let min:Double = Double(timeString.components(separatedBy: ":")[0])!
        let range = NSMakeRange(3, 2)
        let second:Double = Double((timeString as NSString).substring(with: range))!
        let haomiao:Double = Double(timeString.components(separatedBy: ".")[1])!
        let timer1 = min*60
        let timer2 = haomiao*0.01
        return (Float)(timer1+timer2+second)
    }
    func getLabWidth(_ labelStr:String,font:UIFont,height:CGFloat) -> CGFloat {
        let statusLabelText: NSString = labelStr as NSString
        let size = CGSize(width: APPW, height: height)
        let dic = NSDictionary(object: font, forKey: NSFontAttributeName as NSCopying)
        let strSize = statusLabelText.boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: dic as? [String : AnyObject], context:nil).size
        return strSize.width
    }
    
    @IBAction func clickClosebtn() {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK: - UITableViewDelegate，UITableViewDataSource
extension SongViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lrcArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ID = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: ID)
        if cell == nil {
            cell = MyLrcTableViewCell(style: .default, reuseIdentifier: ID)
        }
        (cell as! MyLrcTableViewCell).selectionStyle = .none
        cell?.backgroundColor = UIColor.clear
        (cell as! MyLrcTableViewCell).lrcLable.text = lrcArr[(indexPath as NSIndexPath).row]
        
        (cell as! MyLrcTableViewCell).lrcLable.cl_centerX = self.lrcTableView.cl_centerX
        
        if(self.currentIndex == (indexPath as NSIndexPath).row){
            (cell as! MyLrcTableViewCell).lrcLable.font = UIFont.systemFont(ofSize: 25)
            let str = (cell as! MyLrcTableViewCell).lrcLable.text
            let lableW = self.getLabWidth(str!, font: UIFont.systemFont(ofSize: 25), height: 30)
            (cell as! MyLrcTableViewCell).lrcLable.cl_width = lableW
            (cell as! MyLrcTableViewCell).lrcLable.cl_centerX = self.lrcTableView.cl_centerX
            (cell as! MyLrcTableViewCell).lrcLable.textColor = UIColor.white
            
            (cell as! MyLrcTableViewCell).lrcLable.adjustsFontSizeToFitWidth = true
            (cell as! MyLrcTableViewCell).lrcLable.minimumScaleFactor = 0.1
        } else {
            (cell as! MyLrcTableViewCell).lrcLable.cl_width = APPW  // 防止cell宽度复用
            (cell as! MyLrcTableViewCell).lrcLable.cl_centerX = self.lrcTableView.cl_centerX
            (cell as! MyLrcTableViewCell).cellprogress = 0
            (cell as! MyLrcTableViewCell).lrcLable.font = UIFont.systemFont(ofSize: 15)
            (cell as! MyLrcTableViewCell).lrcLable.textColor = UIColor.white
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lrcTableView.scrollToRow(at: indexPath, at: .middle, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //  当前行
        let scrllCurrentLine = Int(((scrollView.contentOffset.y+lrcTableView.cl_height*0.5)/40)+lrcTableView.cl_height*0.5/40)
        print(scrllCurrentLine)
        // 这个一行的时间
        if scrllCurrentLine<lycTimerArr.count{
            let scrllCurrentLineTimeStr:String = lycTimerArr[scrllCurrentLine]
            let scrllCurrentLineTime = self.timeStringWithString(scrllCurrentLineTimeStr)
            
            MusicOperationTool.shareInstance.tool.player!.seek(to: CMTimeMake(Int64(scrllCurrentLineTime),1))
            
            self.setcurrentTime(scrllCurrentLineTime)
        }
    }
}
//MARK: - 歌词的解析
extension SongViewController{
    // 初始化文件路径
    fileprivate func setupLrcArr(_ model:musicModel){
        
        // 歌词
        let urlstr = Bundle.main.url(forResource: model.LRC, withExtension: nil)
        let lrcStr = try? String(contentsOf: urlstr!, encoding: String.Encoding.utf8)
        let lrcArray = lrcStr?.components(separatedBy: "\n")
        
        // 取出不含时间的文字
        var mutArr:[String] = []
        var mutArr2:[String] = []
        
        var totalArr:[String] = []
        
        for lrc in lrcArray! {
            let titleArr = lrc.components(separatedBy: "]")
            
            for i in 0..<titleArr.count-1{
                let lineLrc = titleArr[i] + "]" + titleArr.last!
                totalArr.append(lineLrc)
            }
        }
        
        // 对totalArr进行排序
        totalArr.sort { (re1, re2) -> Bool in
            var strTimer1:Float = 0
            var strTimer2:Float = 0
            if re1.contains(".")==true&&re2.contains(".")==true{
                let range = NSMakeRange(1, 8)
                let str1 = (re1 as NSString).substring(with: range)
                strTimer1 = self.timeStringWithString(str1)
                let str2 = (re2 as NSString).substring(with: range)
                strTimer2 = self.timeStringWithString(str2)
            }
            return strTimer1<strTimer2
        }
        
        for lrc2 in totalArr {
            let titleArr = lrc2.components(separatedBy: "]")
            let timerStr = titleArr.first!
            mutArr2.append(timerStr)
            var titleStr = titleArr.last!
            if titleStr=="\r"&&timerStr.hasPrefix("[ti:")==false&&timerStr.hasPrefix("[ar:")==false&&timerStr.hasPrefix("[al:")==false&&timerStr.hasPrefix("[by:")==false{
                mutArr.append("*雪峰音乐*")
            }
            if titleStr.characters.count<2 {
                continue
            }
            let titleStr2 = (titleStr as NSString).substring(to: titleStr.characters.count-1)
            mutArr.append(titleStr2)
        }
        
        // 取出时间
        var mutArr3:[String] = []
        for i in 0..<mutArr2.count {
            
            let lrctimer = mutArr2[i]
            
            if lrctimer.hasPrefix("[ti:")||lrctimer.hasPrefix("[ar:")||lrctimer.hasPrefix("[al:")||lrctimer.hasPrefix("[by:") {
                continue
            }
            let lyr = lrctimer.substring(from: lrctimer.characters.index(lrctimer.startIndex, offsetBy: 1))
            mutArr3.append(lyr)
        }
        
        self.lrcArr = mutArr
        self.lycTimerArr = mutArr3
        
        // 开启歌词的定时器
        self.startLrcTime()
        self.lrcTableView.reloadData()
    }
    
    //开启歌词定时器
    fileprivate func startLrcTime(){
        
        lrcTimer = CADisplayLink.init(target: self, selector: #selector(SongViewController.updataLrc))
        lrcTimer.add(to: RunLoop.main, forMode: RunLoopMode.commonModes)
    }
    fileprivate func stopLrcTime(){
        self.startLrcTime()
        lrcTimer.invalidate()
    }
    @objc fileprivate func updataLrc(){
        let songItem = MusicOperationTool.shareInstance.tool.player?.currentItem
        let currentT = songItem?.currentTime()
        let currentTime = CMTimeGetSeconds(currentT!)
        
        self.setcurrentTime(Float(currentTime))
    }
    
    fileprivate func setcurrentTime(_ currentTime:Float){
        let currentTime = currentTime
        // 当前播放的时间  4.467947662
        let minute:Int = Int(currentTime) / 60;
        let second:Int = Int(currentTime) % 60;
        let msecond:Int = Int(currentTime*100) - Int(currentTime)*100
        let currentTimeStr = String(format: "%02d:%02d.%02d",arguments:[minute,second,msecond])
        
        let currentShijian = self.timeStringWithString(currentTimeStr)
        
        let count = lycTimerArr.count
        for index in 0..<count {
            // 时间数组中每行的时间
            let currentLineTime:String = lycTimerArr[index]
            
            // 下一行的时间
            var nextLineTime = String()
            let nextIdx = index + 1
            var nextLineTimeDoubla:Float = 0
            
            if nextIdx < lycTimerArr.count {
                nextLineTime = lycTimerArr[nextIdx]
                nextLineTimeDoubla = self.timeStringWithString(nextLineTime)
            }
            if currentIndex == index {
                // 拿到当期位置的cell
                let indexPath = IndexPath.init(item: currentIndex, section: 0)
                let cell = self.lrcTableView.cellForRow(at: indexPath) as? MyLrcTableViewCell
                
                let currentLineStr:String = lycTimerArr[currentIndex]
                let currentLineTimer = self.timeStringWithString(currentLineStr)
                
                // 更新进度
                let progress = (currentShijian-currentLineTimer)/(nextLineTimeDoubla-currentLineTimer)
                cell?.cellprogress = CGFloat(progress)
            }
            
            if (currentTimeStr.compare(currentLineTime)==ComparisonResult.orderedAscending)==false && currentTimeStr.compare(nextLineTime)==ComparisonResult.orderedAscending && (currentIndex==index)==false {
                // 刷新tableView
                let reloadRows = [IndexPath.init(row: currentIndex, section: 0),IndexPath.init(row: index, section: 0)]
                
                currentIndex = index
                lrcTableView.reloadRows(at: reloadRows, with: .none)
                let indexPath = IndexPath.init(row: index, section: 0)
                lrcTableView.scrollToRow(at: indexPath, at: .middle, animated: true)
            }
            
            
            for index2 in 0..<count {
                let indexPath1 = IndexPath.init(item: index2, section: 0)
                let cell1 = self.lrcTableView.cellForRow(at: indexPath1) as? MyLrcTableViewCell
                cell1?.lrcLable.cl_centerX = self.lrcTableView.cl_centerX

            }
        }
    }
    
}
